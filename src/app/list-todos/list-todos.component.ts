import { TodoDataService } from './../service/data/todo-data.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

export class Todo {
  constructor(
    public id: number,
    public description: string,
    public done: boolean,
    public targetDate: Date
  ){

  }
}

@Component({
  selector: 'app-list-todos',
  templateUrl: './list-todos.component.html',
  styleUrls: ['./list-todos.component.css']
})
export class ListTodosComponent implements OnInit {

  todos: Todo[]

  message: string

  columnDefs = [
      {headerName: 'id', field: 'id', sortable: true, filter: true, checkboxSelection: true },
        {headerName: 'Description', field: 'description', sortable: true, filter: true },        
        {headerName: 'is Completed?', field: 'done', sortable: true, filter: true },
        {headerName: 'Target Date', field: 'targetDate', sortable: true, filter: true }
  ]; 

  rowData: any;

  // rowData = [
  //       { id: 1, description: 'Toyota', done: false},
  //       { description: 'Toyota', targetDate: 'Cfalseelica', done: '35000'},
  //       { description: 'Toyota', targetDate: 'Celica', done: '35000'},
  //       { description: 'Toyota', targetDate: 'Celica', done: '35000'},
  //       { description: 'Toyota', targetDate: 'Celica', done: '35000'}
  // ];

  constructor(
    private todoService:TodoDataService,
    private router : Router
  ) { }

  ngOnInit() {
    //this.rowData = this.todos;
    this.refreshTodos();
  }

  refreshTodos(){
    this.todoService.retrieveAllTodos('demo').subscribe(
      response => {
        console.log(response);
        this.todos = response;
        this.rowData = response;
      }
    )
  }

  deleteTodo(id) {
    console.log(`delete todo ${id}` )
    this.todoService.deleteTodo('demo', id).subscribe (
      response => {
        console.log(response);
        this.message = `Delete of Todo ${id} Successful!`;
        this.refreshTodos();
      }
    )
  }

  updateTodo(id) {
    console.log(`update ${id}`)
    this.router.navigate(['todos',id])
  }

  addTodo() {
    this.router.navigate(['todos',-1])
  }
}
